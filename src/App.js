import { useState } from "react"
import './App.css';

function App() {
  const [count, setCount] = useState(0);
  const [choose, setOne] = useState("Choose")
  return (
    <div className="App">
      <header className="App-header">
        <div>
          <h2>Click Here!</h2>
          <h3>{count}</h3>
          <button className="App-button" onClick={() => setCount(count + 1)}>+</button>
          <button className="App-button" onClick={() => setCount(count - 1)}>-</button>
        </div>
        <div>
          <h2>Do you like BTS?</h2>
          <h4>{choose}</h4>
          <button className="App-button" onClick={() => setOne('BESTIE!!!')}>YES</button>
          <button className="App-button" onClick={() => setOne('YOU CHOOSE THE WRONG BUTTON')}>NO</button>
        </div>
      </header>
    </div>
  );
}

export default App;
